<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuestionRequest;
use App\Questions\Exceptions\UnavailableFormat;
use App\Questions\Factory;
use Illuminate\Http\Response;

class QuestionsController extends Controller
{

    private $factory;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    public function store(QuestionRequest $request)
    {
        try {
            $type = $request->get('type');
            $content = $request->get('content');

            $strategy = $this->factory->build($type);
            $questions = $strategy->buildQuestion($content);

            return response()->json([
                'message' => sprintf('%u question(s) have been created successfully', count($questions))
            ]);
        } catch (UnavailableFormat $error) {
            return response()->json($error->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }
}
