<?php

namespace App\Http\Requests;

use App\Questions\Upload\QuestionStrategyInterface;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class QuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => [
                'required', Rule::in(QuestionStrategyInterface::ALLOWED_TYPES)
            ],
            'content' => 'required'
        ];
    }
}
