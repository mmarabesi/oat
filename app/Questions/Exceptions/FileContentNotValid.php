<?php

namespace App\Questions\Exceptions;

use Exception;
use Throwable;

class FileContentNotValid extends Exception
{

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $concat = sprintf('File content provided is not valid %s', $message);
        parent::__construct($concat, $code, $previous);
    }
}
