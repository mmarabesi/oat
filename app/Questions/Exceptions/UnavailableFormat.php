<?php

namespace App\Questions\Exceptions;

use Exception;
use Throwable;

class UnavailableFormat extends Exception
{
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        $concat = sprintf('The specified format is invalid (%s)', $message);
        parent::__construct($concat, $code, $previous);
    }
}
