<?php

namespace App\Questions;

use App\Questions\Exceptions\UnavailableFormat;
use App\Questions\Upload\QuestionStrategyInterface;

class Factory
{

    /**
     * @param string $type
     * @return QuestionStrategyInterface
     * @throws UnavailableFormat
     */
    public function build(string $type): QuestionStrategyInterface
    {
        $strategy = ucfirst($type);
        $fullClassName = sprintf('%s\\Upload\\%s', __NAMESPACE__, $strategy);

        if (!class_exists($fullClassName)) {
            throw new UnavailableFormat($type);
        }

        return new $fullClassName;
    }
}
