<?php

namespace App\Questions\Types;

class Choice
{

    private $text = '';

    public function __construct(string $text = '')
    {
        $this->text = $text;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text)
    {
        $this->text = $text;

        return $this;
    }
}
