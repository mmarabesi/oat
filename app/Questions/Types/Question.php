<?php

namespace App\Questions\Types;

class Question
{

    private $text = '';
    private $choices = [];
    private $createdAt = '';

    public function __construct(string $text = '', string $createdAt = '', array $choices = [])
    {
        $this->text = $text;
        $this->createdAt = $createdAt;
        $this->choices = $choices;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text)
    {
        $this->text = $text;

        return $this;
    }

    public function getChoices(): array
    {
        return $this->choices;
    }

    public function setChoices(array $choices)
    {
        $this->choices = $choices;

        return $this;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function setCreatedAt(string $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}