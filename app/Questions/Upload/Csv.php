<?php

namespace App\Questions\Upload;

use App\Questions\Types\Choice;
use App\Questions\Types\Question;
use League\Csv\Reader;

class Csv implements QuestionStrategyInterface
{
    public function buildQuestion(string $content): array
    {
        $reader = Reader::createFromString($content);
        $reader->setHeaderOffset(0);

        $questions = [];

        foreach($reader->getRecords() as $record) {
            $question = new Question($record['Question text'], $record['Created At']);

            $question->setChoices([
                new Choice($record['Choice 1']),
                new Choice($record['Choice 2']),
                new Choice($record['Choice 3']),
            ]);

            $questions[] = $question;
        }

        return $questions;
    }
}
