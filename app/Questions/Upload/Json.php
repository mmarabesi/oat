<?php

namespace App\Questions\Upload;

use App\Questions\Types\Choice;
use App\Questions\Types\Question;

class Json implements QuestionStrategyInterface
{
    public function buildQuestion(string $content): array
    {
        $parsedContent = json_decode($content, true);
        $questions = [];

        foreach($parsedContent as $record) {
            $question = new Question($record['text'], $record['createdAt']);
            $choices = [];

            foreach ($record['choices'] as $choice) {
                $choice[] = new Choice($choice['text']);
            }

            $question->setChoices($choices);
            $questions[] = $question;
        }


        return $questions;
    }
}
