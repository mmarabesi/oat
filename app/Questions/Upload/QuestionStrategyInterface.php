<?php

namespace App\Questions\Upload;

interface QuestionStrategyInterface
{

    public const CSV = 'csv';
    public const JSON = 'json';

    public const ALLOWED_TYPES = [
        self::CSV,
        self::JSON
    ];

    public function buildQuestion(string $content): array;
}
