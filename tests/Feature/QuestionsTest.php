<?php

namespace Tests\Feature;

use App\Questions\Upload\QuestionStrategyInterface;
use Illuminate\Http\Response;
use Tests\TestCase;

class QuestionsTest extends TestCase
{

    public function invalidTypes(): array
    {
        return [
            ['xml'],
            ['txt'],
            ['']
        ];
    }

    /**
     * @param string $type file type upload
     * @dataProvider invalidTypes
     */
    public function testRejectInvalidTypes(string $type)
    {
        $response = $this->post(route('questions.store'), [
            'type' => $type,
        ]);

        $response->assertStatus(Response::HTTP_FOUND);
    }

    public function testShouldUploadCsvContent()
    {
        $csv = file_get_contents(__DIR__ . '/../Fixtures/questions.csv');
        $response = $this->post(route('questions.store'), [
            'type' => QuestionStrategyInterface::CSV,
            'content' => $csv,
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'message' => '2 question(s) have been created successfully'
        ]);
    }

    public function testShouldUploadJsonContent()
    {
        $json = file_get_contents(__DIR__ . '/../Fixtures/questions.json');
        $response = $this->post(route('questions.store'), [
            'type' => QuestionStrategyInterface::JSON,
            'content' => $json,
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'message' => '2 question(s) have been created successfully'
        ]);
    }
}
