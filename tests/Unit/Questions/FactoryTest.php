<?php

namespace Tests\Unit\Questions;

use App\Questions\Exceptions\UnavailableFormat;
use App\Questions\Factory;
use App\Questions\Upload\QuestionStrategyInterface;
use Tests\TestCase;

class FactoryTest extends TestCase
{

    public function validStrategies()
    {
        return [
            [QuestionStrategyInterface::CSV],
            [QuestionStrategyInterface::JSON],
        ];
    }

    /**
     * @param string $type
     * @dataProvider validStrategies
     * @throws UnavailableFormat
     */
    public function testCreateObjectBasedOnStrategy(string $type)
    {
        $factory = new Factory();
        $strategy = $factory->build($type);

        $this->assertInstanceOf(QuestionStrategyInterface::class, $strategy);
    }

    public function testThrowExceptionInvalidStrategy()
    {
        $this->expectException(UnavailableFormat::class);

        $factory = new Factory();
        $factory->build('invalid');
    }
}
