<?php

namespace Tests\Unit\Questions\Types;

use App\Questions\Types\Choice;
use Tests\TestCase;

class ChoicesTest extends TestCase
{

    public function testBuildEmptyChoicesObject()
    {
        $choices = new Choice();
        $this->assertEquals($choices->getText(), '');
    }

    public function testBuildObjectViaConstructParams()
    {
        $choices = new Choice('my text');
        $this->assertEquals($choices->getText(), 'my text');
    }
}
