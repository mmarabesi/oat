<?php

namespace Tests\Unit\Questions\Types;

use App\Questions\Types\Question;
use Tests\TestCase;

class QuestionTest extends TestCase
{

    public function testBuildEmptyQuestionObject()
    {
        $question = new Question();
        $this->assertEquals($question->getText(), '');
        $this->assertEquals($question->getCreatedAt(), '');
        $this->assertEquals(count($question->getChoices()), 0);
    }

    public function testSetChoicesToAQuestion()
    {
        $question = new Question();
        $question->setChoices([
            'a',
            'b',
            'c'
        ]);
        $this->assertEquals(count($question->getChoices()), 3);
    }

    public function testShouldBuildQuestionObjectViaConstructParams()
    {
        $question = new Question('my', '2019-11-10');
        $this->assertEquals($question->getText(), 'my');
        $this->assertEquals($question->getCreatedAt(), '2019-11-10');
    }

    public function testShouldAttachChoicesToAquestion()
    {
        $question = new Question('', '', ['a', 'b']);
        $this->assertEquals(count($question->getChoices()), 2);
    }
}
