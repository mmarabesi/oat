<?php

namespace Tests\Unit\Upload;

use Tests\TestCase;
use App\Questions\Upload\Csv;

class CsvTest extends TestCase
{
    public function csvProvider()
    {
        return [
            [
                <<<CSV
                "Question text", "Created At", "Choice 1", "Choice 2", "Choice 3"
                "What is the capital of Luxembourg ?", "2019-06-01 00:00:00", "Luxembourg", "Paris", "Berlin"
                "What does mean O.A.T. ?", "2019-06-02 00:00:00", "Open Assignment Technologies", "Open Assessment Technologies", "Open Acknowledgment Technologies"
                CSV,
                2
            ],
            [
                <<<CSV
                "Question text", "Created At", "Choice 1", "Choice 2", "Choice 3"
                "What is the capital of Luxembourg ?", "2019-06-01 00:00:00", "Luxembourg", "Paris", "Berlin"
                CSV,
                1
            ],
        ];
    }

    /**
     * @dataProvider csvProvider
     * @param string $csvContent
     * @param int $expected
     */
   public function testParseCsvContentIntoQuestions(string $csvContent, int $expected)
    {
        $csv = new Csv();
        $questions = $csv->buildQuestion($csvContent);

        $this->assertEquals(count($questions), $expected);
    }
}
