<?php

namespace Tests\Unit\Upload;

use Tests\TestCase;
use App\Questions\Upload\Json;

class JsonTest extends TestCase
{
    public function csvProvider()
    {
        return [
            [
                <<<'JSON'
                  [
                    {
                      "text": "What is the capital of Luxembourg ?",
                      "createdAt": "2019-06-01 00:00:00",
                      "choices": [
                        {
                          "text": "Luxembourg"
                        },
                        {
                          "text": "Paris"
                        },
                        {
                          "text": "Berlin"
                        }
                      ]
                    },
                    {
                      "text": "What does mean O.A.T. ?",
                      "createdAt": "2019-06-02 00:00:00",
                      "choices": [
                        {
                          "text": "Open Assignment Technologies"
                        },
                        {
                          "text": "Open Assessment Technologies"
                        },
                        {
                          "text": "Open Acknowledgment Technologies"
                        }
                      ]
                    }
                  ]
                JSON,
                2
            ],
        ];
    }

    /**
     * @dataProvider csvProvider
     * @param string $jsonContent
     * @param int $expected
     */
    public function testParseJsonContentIntoQuestions(string $jsonContent, int $expected)
    {
        $json = new Json();
        $questions = $json->buildQuestion($jsonContent);

        $this->assertEquals(count($questions), $expected);
    }
}
